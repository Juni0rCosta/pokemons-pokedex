import styled from 'styled-components'

export const Button = styled.button`
  position: relative;
  bottom: 0;
  max-width: 100%;
  height: 30.4rem;
  background-color: #ffffff;
  border: 0;
  -webkit-box-shadow: 0px 10px 51px -5px rgba(183, 189, 193, 0.3);
  box-shadow: 0px 10px 51px -5px rgba(183, 189, 193, 0.3);
  border-radius: 12px;
  cursor: pointer;
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
  -webkit-animation: fadeIn 0.3s forwards;
  animation: fadeIn 0.3s forwards;
  &:hover {
    -webkit-box-shadow: 0px 12px 40px -5px rgba(90, 96, 100, 0.3);
    box-shadow: 0px 12px 40px -5px rgba(90, 96, 100, 0.3);
    bottom: 2px;
  }
`

export const Loadmore = styled.button`
  border: 0;
  background-color: rgba(63, 93, 179, 0.1);
  width: 19.6rem;
  line-height: 4.5rem;
  display: block;
  margin: 0 auto;
  margin-top: 6.8rem;
  font-weight: 600;
  font-size: 1.4rem;
  letter-spacing: -0.01em;
  color: #3f5db3;
  border-radius: 6px;
  cursor: pointer;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
  &:hover {
    background-color: #3f5db3;
    color: #ffffff;
  }
`
export const Name = styled.div`
  font-family: 'Montserrat', sans-serif;
  font-weight: 600;
  font-size: 1.8rem;
  line-height: 150%;
  color: #2f3133;
  text-transform: capitalize;
`
export const Img = styled.div`
position:relative;
max-height
justify-content:center;`

