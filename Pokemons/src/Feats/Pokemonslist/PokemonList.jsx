import { useState, useEffect } from 'react'
import { Button, Loadmore, Name } from './styles'

export default function PokemonsList({
  openModal,
  pokemonList,
  setPokemonId,
  visible,
  setVisible
}) {
  const handleClick = e => {
    setVisible(visible + 3)
  }
  const setId = id => {
    setPokemonId(id)
    openModal()
  }//modal id for compatible data

  return (
    <>
      <div className="all">
        {pokemonList?.slice(0, visible).map((item, index) => {
          const data = item.info
          return (
            <Button key={index} onClick={() => setId(data.id)}>
              <div className="image">
                <img
                  src={data.sprites.other.dream_world.front_default}
                  alt={data.name}
                  style={{ maxHeight: '20rem' }}
                />
              </div>
              <div className="info">
                <div>
                  <span>#{data.id}</span>
                  <Name>{data.name}</Name>
                </div>
                <div className="icon"></div>
              </div>
            </Button>
          )
        })}
      </div>
      <Loadmore onClick={handleClick}>Show more Pokémons</Loadmore>
    </>
  )
}
