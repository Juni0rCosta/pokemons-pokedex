import mglass from '../assets/images/mglass.svg'
import { useRef } from 'react'

export default function Search({ setSearchTerm }) {
  const searchValue = useRef(null)

  const handleSubmit = e => {
    e.preventDefault()
    setSearchTerm(searchValue.current.value)
    searchValue.current.value = ""
  }

  return (
    <form className="search" onSubmit={handleSubmit}>
      <input type="text" placeholder="Search Pokémon" ref={searchValue} />
      <button type="submit" className="icon">
        <img src={mglass} alt="magnifying glass" />
      </button>
    </form>
  )
}
