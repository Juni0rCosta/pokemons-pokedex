import close from '../assets/images/close.svg'
import '../App.css'
import { useEffect, useState } from 'react'

export default function Modal({
  closeModal,
  isModalOpen,
  pokemonId,
  pokemonList
}) {
  const [newPokemon, setNewPokemon] = useState([])
  const pokemonUrl = `https://pokeapi.co/api/v2/pokemon/${pokemonId}`
  

  const fetchPokemon = async () => {
    try {
      const response = await fetch(pokemonUrl)
      const newPokemon = await response.json()
      setNewPokemon(newPokemon)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    fetchPokemon()
  }, [isModalOpen])

  const { name, types, height, weight, abilities, stats } = newPokemon

  return (
    <div className="modal" id={`${isModalOpen ? 'open-modal' : ''}`}>
      <div className="overlay"></div>
      <div className="box">
        <button className="close js-close-details-pokemon" onClick={closeModal}>
          <img src={close} alt="close" title="close" />
        </button>
        <div className="left-container"></div>
        <div className="right-container">
          <div className="name">
            <h2>{name}</h2>
            <span>#{pokemonId}</span>
          </div>

          <ul className="type">
            {types?.map((item, id) => {
              return (
                <li className="tag" key={id}>
                  {item.type.name}
                </li>
              )
            })}
          </ul>

          <ul className="info">
            <li>Height:{height / 10}m</li>
            <li>Weight:{weight / 10}Kg</li>
            <ul>
              Abilities
              {abilities?.map((item, id) => {
                return <li key={id}>{item.ability.name}</li>
              })}
            </ul>
          </ul>
          <div className="weak">
            <h4>weaknesses</h4>
            <ul>
              <li className="tag">wk1</li>
              <li className="tag">wk2</li>
              <li className="tag">wk3</li>
            </ul>
          </div>
          <div className="stat">
            <div className="item">
              {' '}
              <span>HP {stats?.[0].base_stat}</span>{' '}
            </div>
            <div className="item">
              <span>Attack {stats?.[1].base_stat} </span>
            </div>
            <div className="item">
              <span>Defense {stats?.[2].base_stat} </span>
            </div>
            <div className="item">
              <span>Sp. attack {stats?.[3].base_stat}</span>
            </div>
            <div className="item">
              <span>Sp. defense {stats?.[4].base_stat}</span>
            </div>
            <div className="item">
              <span>Speed {stats?.[5].base_stat}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
