import styled from 'styled-components'

export const StyledLi = styled.li`
  display: flex;
  gap: 1rem;
`

export const Button = styled.span`
  text-transform: capitalize;
  opacity: 0.6;
  -webkit-filter: grayscale(100%);
  filter: grayscale(100%);
  border: 0;
  background-color: transparent;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  cursor: pointer;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
  font-size: 2rem;
`

export const StyledUl = styled.ul`
  float: right;
  display: flex;
  flex-direction: column;
  padding-right: 13rem;
`
export const IconUl = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  filter: grayscale(100%);
  &:hover {
    opacity: 1;
    filter: grayscale(0%);
    transition: all 0.3s;
  }
`
export const Poketype = styled.button`
  max-width: 27px;
  margin-top: 10px;
  gap:2rem;
`
