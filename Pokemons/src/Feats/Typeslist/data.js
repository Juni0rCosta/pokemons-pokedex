const icons = [
  { id: 1,"type":"all", img: "https://i.ibb.co/0XDxLRJ/0-All.png"},
  { id: 2,"type":"normal", img: 'https://i.ibb.co/K28pmFd/1-Normal.png' },
  { id: 3,"type":"fighting", img: 'https://i.ibb.co/hV2nF9p/2-Fighting.png' },
  { id: 4,"type":"flying", img: 'https://i.ibb.co/DLXWPh7/3-Flying.png' },
  { id: 5,"type":"poison", img: 'https://i.ibb.co/hLqntZC/4-Poison.png' },
  { id: 6,"type":"ground", img: 'https://i.ibb.co/h7vrY28/5-Ground.png' },
  { id: 7,"type":"rock", img: 'https://i.ibb.co/wpntbVM/6-Rock.png' },
  { id: 8,"type":"bug", img: 'https://i.ibb.co/zWdWkQy/7-Bug.png' },
  { id: 9,"type":"ghost", img: 'https://i.ibb.co/Ycqj6fP/8-Ghost.png' },
  { id: 10,"type":"steel", img: 'https://i.ibb.co/Qv07CRf/9-Steel.png' },
  { id: 11,"type":"fire", img: 'https://i.ibb.co/LQb8FkP/10-Fire.png' },
  { id: 12,"type":"water", img: 'https://i.ibb.co/gSJjrTT/11-Water.png' },
  { id: 13,"type":"grass", img: 'https://i.ibb.co/X2zRth7/12-Grass.png' },
  { id: 14,"type":"electric", img: 'https://i.ibb.co/NyJ9m2w/13-Electric.png' },
  { id: 15,"type":"psychic", img: 'https://i.ibb.co/J3xF475/14-Psychic.png' },
  { id: 16,"type":"ice", img: 'https://i.ibb.co/KG6kxfw/15-Ice.png' },
  { id: 17,"type":"dragon", img: 'https://i.ibb.co/NC8ZZBL/16-Dragon.png' },
  { id: 18,"type":"dark", img: 'https://i.ibb.co/jL44mjQ/17-Dark.png' },
  { id: 19,"type":"fairy", img: 'https://i.ibb.co/DtTvh2r/18-Fairy.png' }
]

export default icons
