import { useEffect, useState } from 'react'
import { StyledLi, Button, Poketype, StyledUl, IconUl } from './styles'
import icons from './data'
import { useRef } from 'react'

export default function TypesList({
  fetchPokemonData,
  actualType,
  setActualType,
  setPokemonList,
  setVisible,
  visible
}) {
  const fetchPokemonByType = async () => {
    const response = await fetch(`https://pokeapi.co/api/v2/type/${actualType}`)
    const getInitialPokemons = await response.json()

    const filteredByType = await Promise.all(
      getInitialPokemons.pokemon.map(async item => {
        item.info = await fetch(item.pokemon.url).then(results =>
          results.json()
        )
        return item
      })
    )
    setPokemonList(filteredByType)
  }

  useEffect(() => {
    if (actualType === 'all') {
      setVisible(9)
      fetchPokemonData()
    } else {
      setVisible(9)
      fetchPokemonByType()
    }
    console.log(actualType)
  }, [actualType, visible])
  return (
    <>
      <IconUl>
        {icons.map((item, index) => {
          return (
            <StyledLi key={index} onClick={() => setActualType(item.type)}>
              <Poketype key={index}>
                <img src={item.img} alt="" />
                <Button>{item.type}</Button>
              </Poketype>
            </StyledLi>
          )
        })}
      </IconUl>
    </>
  )
}
