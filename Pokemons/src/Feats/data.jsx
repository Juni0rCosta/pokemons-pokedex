const slides = [{
  id:1,
  bg:"https://codeboost.com.br/projetos/pokeapi/img/bg-red.svg",
  title:"Who is that Pokémon?",
  img:"https://i.ibb.co/wrJShyP/pokeball-transparente.png",
},
{
  id:2,
  bg:"https://codeboost.com.br/projetos/pokeapi/img/bg-blue.svg",
  title:"Catch them all!",
  img:"https://i.ibb.co/dMpy4HD/superball-transparente.png",
},
{
  id:3,
  bg:"https://static.vecteezy.com/system/resources/previews/002/421/227/original/purple-lavender-color-background-free-vector.jpg",
  title:"Be the Pokémon Master!",
  img:"https://i.ibb.co/JvqSjJM/masterball-transparente.png",
}
]

export default slides



