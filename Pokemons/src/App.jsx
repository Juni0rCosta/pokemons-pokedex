import { useState } from 'react'

import './App.css'
import Footer from './Components/Footer'
import Header from './Components/Header'
import Main from './Components/Main'
import Slider from './Components/Slider'

function App() {
  return (
    <div className="App">
      <Header />
      <Slider />
      <Main />
      <Footer/>
    </div>
  )
}

export default App
