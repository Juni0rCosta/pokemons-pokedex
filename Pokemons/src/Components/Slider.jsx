import { useEffect, useState } from 'react'

import Slides from './slides/Slides'

export default function Slider() {
  return (
    <section className="s-area-slide-hero">
      <div className="slide-hero swiper-container-fade swiper-container-initialized swiper-container-horizontal">
        <div className="swiper-wrapper ">
          <Slides />
        </div>
      </div>
    </section>
  )
}
