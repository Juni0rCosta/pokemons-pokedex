
import logo1 from '../assets/images/Logo.svg'

export default function Header(){
  return (
    <header>
        <div className="container">
          <img src={logo1} alt="pokemon logo" />
        </div>
      </header>
  )
}