import { useEffect, useState } from 'react'
import iconRed from '../assets/images/icon-poke-red.svg'
import Modal from '../Feats/Modal'
import PokemonsList from '../Feats/Pokemonslist/PokemonList'
import Search from '../Feats/Search'
import TypesList from '../Feats/Typeslist/TypesList'

export default function Main() {
  const [searchTerm, setSearchTerm] = useState(null)
  const [isModalOpen, setIsModalOpen] = useState(false)

  const [visible, setVisible] = useState(9)
  const [offset, setOffset] = useState(0)

  const [pokemonList, setPokemonList] = useState([])
  const [pokemonId, setPokemonId] = useState('')
  const [actualType, setActualType] = useState('all')

  const fetchPokemonData = async () => {
    const response = await fetch(
      `https://pokeapi.co/api/v2/pokemon?limit=${visible}&offset=${offset}`
    )

    const getInitialPokemons = await response.json()

    const enrichedPokemons = await Promise.all(
      getInitialPokemons.results.map(async item => {
        item.info = await fetch(item.url).then(results => results.json())
        return item
      })
    )

    setPokemonList(enrichedPokemons)
  } //pokemon data fetch

  const fetchPokemonByTerm = async () => {
    const response = await fetch(
      `https://pokeapi.co/api/v2/pokemon?limit=905&offset=0`
    )
    const getInitialPokemons = await response.json()

    const searchResult = await Promise.all(
      getInitialPokemons.results
        .filter(list => list.name.includes(searchTerm))
        .map(async item => {
          item.info = await fetch(item.url).then(results => results.json())
          return item
        })
    )
    setPokemonList(searchResult)
  } //search fetch

  useEffect(() => {
    if (!searchTerm) {
      fetchPokemonData()
    }
    if (searchTerm) {
      setVisible(9)
      fetchPokemonByTerm()
    }
  }, [searchTerm]) //search + setVisible if search, fetch data if not

  useEffect(() => {
    setVisible(visible)
  }, [visible]) //set visible based on visible change

  const openModal = id => {
    setIsModalOpen(pokemonList?.filter(item => item.id == id))
  }

  const closeModal = () => {
    setIsModalOpen(false)
  }

  return (
    <main className="s-all-info-pokemons">
      <div className="container">
        <div className="top">
          <h2>Select your Pokémon</h2>
          <Search setSearchTerm={setSearchTerm} />
        </div>
        <div className="area-all">
          <div className="left-container">
            <TypesList
              actualType={actualType}
              setActualType={setActualType}
              fetchPokemonData={fetchPokemonData}
              setPokemonList={setPokemonList}
              setVisible={setVisible}
              visible={visible}
            />
          </div>
          <div className="right-container">
            <div className="top-container">
              <div>
                <img src={iconRed} />
                <span className="js-count-pokemons">{pokemonList?.length}</span>
              </div>
            </div>
            <PokemonsList
              openModal={openModal}
              pokemonList={pokemonList}
              setPokemonId={setPokemonId}
              visible={visible}
              setVisible={setVisible}
            />
          </div>
        </div>
      </div>
      <Modal
        isModalOpen={isModalOpen}
        closeModal={closeModal}
        pokemonId={pokemonId}
        pokemonList={pokemonList}
      />
    </main>
  )
}
