import styled from 'styled-components'

export const StyledH2 = styled.h2`
  font-family: 'Montserrat', sans-serif;
  text-align: center;
  font-weight: bold;
  font-size: 7.5rem;
  line-height: 7.8rem;
  letter-spacing: -0.01em;
  color: #ffffff;
  margin-bottom: 1.2rem;
  line-height: 5.9rem;
`

export const StyledP = styled.p`
  font-weight: 500;
  font-size: 1.8rem;
  line-height: 150%;
  text-align: center;
  color: #ffffff;
`

export const Icon = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  line-height: 3.4rem;
  background-color: #ffffff;
  border-radius: 244px;
  padding: 4px 18px 6px 4px;
  margin-bottom: 2.9rem;
`
export const IconChild = styled.div`
margin-right: 8px;
background-color: #909090;
width: 2.6rem;
align-items: center;
height: 2.6rem;
justify-content: center;
display: flex;
border-radius: 50%;
}`

export const Text = styled.div`
  position: relative;
  height: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: column;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  z-index: 1;
  -webkit-animation: fadeIn 1s forwards;
  
`
export const Image = styled.div`
  position: relative;
  width: 100%;
  max-width: 61rem;
  margin: 0 auto;
  margin-top: 16.2rem;
  animation: float 6s ease-in-out infinite;
`

export const Lights = styled(Image)`
  position: absolute;
  top: -45%;
  left: 50%;
  z-index: 2;
  margin-left: -8.5rem;
`


