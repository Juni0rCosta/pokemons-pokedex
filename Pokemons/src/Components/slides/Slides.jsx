import { useEffect, useState } from 'react'
import luzes from '../../assets/images/luzes.svg'
import arrow from '../../assets/images/arrow-down.svg'
import slides from '../../Feats/data'
import {
  StyledH2,
  StyledP,
  Icon,
  Text,
  Image,
  Lights,
  IconChild
} from './styles'

export default function Slides() {
  const [index, setIndex] = useState(0)

  useEffect(() => {
    let slider = setInterval(() => {
      setIndex(index+1)
      if (index >= 2) setIndex(0)
    }, 10000)

    return () => clearInterval(slider)
  }, [index])
  return (
    <div className={`swiper-slide bg${index+1}`}>
      {slides.map((slide, slideIndex) => {
        let position = 'nextSlide'
        if (slideIndex === index) {
          position = 'activeSlide'
        }
        if (
          slideIndex === index - 1 ||
          (index === 0 && slideIndex === slides.length - 1)
        ) {
          position = 'lastSlide'
        }

        const { id, bg, title, img } = slide
        return (
          <div className={`main area ${position} `} key={id}>
            <div className="container">
              <div className="text">
                <Text>
                  <div className="tag">
                    <Icon>
                      <IconChild>
                        <div className="icon">🎒</div>
                      </IconChild>
                      <span>pokedex</span>
                    </Icon>
                  </div>

                  <StyledH2>{title}</StyledH2>

                  <StyledP>
                    The perfect guide for those who want to hunt Pokémons around
                    the world
                  </StyledP>

                  <Image>
                    <div className="image">
                      <Lights>
                        <img src={luzes} alt="lights"  />
                      </Lights>
                      <img src={img} alt="pokeball" />
                    </div>
                  </Image>
                </Text>
              </div>
              <div className="area-explore">
                <div className="txt">
                  <div className="icon">
                    <img src={arrow} alt="" />
                  </div>
                  <span>explore</span>
                </div>
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}
